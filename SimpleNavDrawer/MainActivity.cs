﻿using Android.App;using Android.Widget;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using System.Collections.Generic;
using System;

namespace SimpleNavDrawer
{
	[Activity (Label = "SimpleNavDrawer", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		private ActionBarDrawerToggle m_DrawerToggle;
		private DrawerLayout m_DrawerLayout;
		private ListView m_DrawerListView;
		private string  [] m_MenuItems = new string  [] {    
			"Home",
			"Option A","Option B","Option C",
			"Frag Uno", "Frag Dos"
		};
		private Dictionary<string,Type> m_Fragments;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.Main);

			m_Fragments = new Dictionary<string, Type>() {
				{"Frag Uno", typeof(FragUno)}, {"Frag Dos", typeof(FragDos)},
				{"Home", typeof(FragHome)}
			};

			m_DrawerLayout = FindViewById<DrawerLayout> (Resource.Id.drawerLayout);
			m_DrawerToggle = new ActionBarDrawerToggle(this, m_DrawerLayout, 
				Resource.String.DrawerOpenDescription, Resource.String.DrawerCloseDescription);

			m_DrawerListView = FindViewById<ListView>(Resource.Id.drawerListView);
			m_DrawerListView.Adapter = new ArrayAdapter<string>(this, Resource.Layout.ListViewMenuRow, 
				Resource.Id.menuRowTextView, m_MenuItems);
			// Set the ActionBarDrawerToggle as a DrawerListener on the DrawerLayout so it receives drawer state-change callbacks
			m_DrawerLayout.SetDrawerListener(m_DrawerToggle);
			// Must up-enable the home button, the ActionBarDrawerToggle will change the icon to the "hamburger"
			ActionBar.SetDisplayHomeAsUpEnabled(true);

			m_DrawerListView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) => OnMenuItemClick(e.Position);
			displayDefaultFragment();
		}

		private void displayDefaultFragment() {
			base.FragmentManager
				.BeginTransaction()
				.Replace(Resource.Id.frameLayout, new FragHome())
				.Commit();
			m_DrawerListView.SetItemChecked(0, true);	// Highlight default item at startup
		}

		void OnMenuItemClick(int position)
		{
			try {
				string sSelected = m_MenuItems[position];
				if(m_Fragments.ContainsKey(sSelected)) {
					Fragment frag = m_Fragments [sSelected].GetConstructor (new Type[] { }).Invoke (new object [] { }) as Fragment;
					base.FragmentManager.BeginTransaction().Replace(Resource.Id.frameLayout, frag).Commit();
					this.Title = sSelected;
				}
				else {
					Toast.MakeText(this,sSelected,ToastLength.Short).Show();
				}
			}
			catch(Exception x) {
				Console.WriteLine (x.Message + "\n" + x.StackTrace);
			}
			finally {
				m_DrawerLayout.CloseDrawer(m_DrawerListView);
			}
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			// Forward all ActionBar-clicks to the ActionBarToggleButton.
			// It will verify if click was on the "Home" button (button at the left edge of the ActionBar).
			// If so, it will toggle the state of the drawer and return "true" so you know you do not need to do any more processing.
			if (m_DrawerToggle.OnOptionsItemSelected(item))
				return true;

			// Other cases go here for other buttons in the ActionBar (not the "home" button)
			//This code is a placeholder to show what would be needed if there were other buttons.
			switch (item.ItemId)
			{
				default: 
					break;
			}
			return base.OnOptionsItemSelected(item);
		}
	}
}


